package item

import "time"

const (
	// Unknown indicates information was not available when the content was
	// discovered.
	Unknown = iota

	// AVC is an encoding value.
	AVC
	// HEVC is an encoding value.
	HEVC
	// Image is a content type.
	Image
	// Video is a content type.
	Video
	// Left is a stereoscopic value.
	Left
	// Right is a stereoscopic value.
	Right
)

// An Item represents a single piece of content.
type Item struct {
	Created time.Time

	// The encoding format.
	Encoding uint
	// The type of content.
	Type uint
	// The collection the item belongs to.
	Group uint
	// The loop value.
	Loop string
	// A ordering value.
	Sequence uint
	// A value indicating the side that was recorded.
	Stereoscopic uint
}
