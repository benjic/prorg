package reader

import (
	"fmt"
	"os"

	"gitlab.com/benjic/prorg/item"
)

const (
	hero6AVCLoopVideo  = "GH%2s%04d.MP4"
	hero6AVCVideo      = "GH%02d%04d.MP4"
	hero6HEVCLoopVideo = "GX%2s%04d.MP4"
	hero6HEVCVideo     = "GX%02d%04d.MP4"
	multiImageFormat   = "G%03d%04d.JPG"
	multiVideoFormat   = "GP%02d%04d.MP4"
	singleImageFormat  = "GOPR%04d.JPG"
	singleVideoFormat  = "GOPR%04d.MP4"
	stereoscopicFormat = "3D_%1s%04d.MP4"
)

var (
	parseName = parseFirst(
		parseType(
			item.Image,
			parsePlural(singleImageFormat, multiImageFormat),
		),
		parseType(
			item.Video,
			parseFirst(
				parseEncoding(
					item.AVC,
					parseFirst(
						parsePlural(singleVideoFormat, multiVideoFormat),
						parseMulti(hero6AVCVideo),
						parseLoop(hero6AVCLoopVideo),
					),
				),
				parseEncoding(
					item.HEVC,
					parseFirst(parseMulti(hero6HEVCVideo), parseLoop(hero6HEVCLoopVideo)),
				),
				parseStereoscopic,
			),
		),
	)
)

type parser func(string) (item.Item, error)

// ParseName yields an Item from a given file name.
func ParseName(name string) (item.Item, error) {
	return parseName(name)
}

// ParseFileInfo yields an Item for a given
func ParseFileInfo(info os.FileInfo) (item.Item, error) {
	i, err := parseName(info.Name())
	i.Created = info.ModTime()

	return i, err
}

func parseFirst(parsers ...parser) parser {
	return func(name string) (i item.Item, err error) {
		for _, parse := range parsers {
			if i, err = parse(name); err == nil {
				return i, err
			}
		}
		return i, err
	}
}

func parseType(fType uint, parser parser) parser {
	return func(name string) (item.Item, error) {
		i, err := parser(name)
		if err != nil {
			return i, err
		}

		i.Type = fType

		return i, nil
	}
}

func parseEncoding(encoding uint, parser parser) parser {
	return func(name string) (item.Item, error) {
		i, err := parser(name)
		if err != nil {
			return i, err
		}

		i.Encoding = encoding

		return i, nil
	}
}

func parsePlural(singleFormat, doubleFormat string) parser {
	return parseFirst(
		parseSingle(singleFormat),
		parseMulti(doubleFormat),
	)
}

func parseSingle(format string) parser {
	return func(name string) (i item.Item, err error) {
		_, err = fmt.Sscanf(name, format, &(i.Sequence))
		return i, err
	}
}

func parseMulti(format string) parser {
	return func(name string) (i item.Item, err error) {
		_, err = fmt.Sscanf(name, format, &(i.Group), &(i.Sequence))
		return i, err
	}
}

func parseLoop(format string) parser {
	return func(name string) (i item.Item, err error) {
		_, err = fmt.Sscanf(name, format, &(i.Loop), &(i.Sequence))
		return i, err
	}
}

func parseStereoscopic(name string) (i item.Item, err error) {
	var side string
	_, err = fmt.Sscanf(name, stereoscopicFormat, &side, &(i.Sequence))

	switch side {
	case "L":
		i.Stereoscopic = item.Left
	case "R":
		i.Stereoscopic = item.Right
	}

	return i, err
}
