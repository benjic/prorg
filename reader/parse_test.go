package reader

import (
	"reflect"
	"testing"

	"gitlab.com/benjic/prorg/item"
)

func Test_ParseName(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name    string
		args    args
		want    item.Item
		wantErr bool
	}{
		{
			"single image",
			args{name: "GOPR0002.JPG"},
			item.Item{Type: item.Image, Group: 0, Sequence: 2},
			false,
		},
		{
			"multi image",
			args{name: "G0231120.JPG"},
			item.Item{Type: item.Image, Group: 23, Sequence: 1120},
			false,
		},
		{
			"single video",
			args{name: "GOPR1002.MP4"},
			item.Item{Encoding: item.AVC, Type: item.Video, Group: 0, Sequence: 1002},
			false,
		},
		{
			"multi video",
			args{name: "GP021234.MP4"},
			item.Item{Encoding: item.AVC, Type: item.Video, Group: 2, Sequence: 1234},
			false,
		},
		{
			"left stereoscopic",
			args{name: "3D_L0002.MP4"},
			item.Item{Type: item.Video, Group: 0, Sequence: 2, Stereoscopic: item.Left},
			false,
		},
		{
			"right stereoscopic",
			args{name: "3D_R0002.MP4"},
			item.Item{Type: item.Video, Group: 0, Sequence: 2, Stereoscopic: item.Right},
			false,
		},
		{
			"hero six AVC",
			args{name: "GH011234.MP4"},
			item.Item{Type: item.Video, Group: 1, Sequence: 1234, Encoding: item.AVC},
			false,
		},
		{
			"hero six HEVC",
			args{name: "GX011234.MP4"},
			item.Item{Type: item.Video, Group: 1, Sequence: 1234, Encoding: item.HEVC},
			false,
		},
		{
			"hero six loop AVC",
			args{name: "GHAA1234.MP4"},
			item.Item{Type: item.Video, Group: 0, Loop: "AA", Sequence: 1234, Encoding: item.AVC},
			false,
		},
		{
			"hero six loop HEVC",
			args{name: "GXAA1234.MP4"},
			item.Item{Type: item.Video, Group: 0, Loop: "AA", Sequence: 1234, Encoding: item.HEVC},
			false,
		},
		{
			"unknown type",
			args{name: "Get_started_with_GoPro.URL"},
			item.Item{Type: item.Unknown, Group: 0, Sequence: 0},
			true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ParseName(tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("parse() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("parse() = %v, want %v", got, tt.want)
			}
		})
	}
}
